﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Anti_Loopback
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string hostsPath = @"C:\Windows\System32\drivers\etc\hosts";

        private void btnSetHost_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            

            if (VerifyIPAddress())
            {
                if (ValidateAddress())
                {
                    
                    if (AddHostsEntry(txtIP.Text, txtAddress.Text, txtComment.Text))
                        MessageBox.Show(null, "Host Entry Added", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show(null, "Host entry Failed", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else
                {
                    MessageBox.Show(null, "Please enter a valid address or ip", "Null address string", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private bool ValidateAddress()
        {
            

            if (txtAddress.TextLength < 0)
                return false;

            try
            {
                if (Uri.CheckHostName(txtAddress.Text) == UriHostNameType.Unknown)
                {
                    string input = txtAddress.Text;
                    txtAddress.Text = Regex.Replace(input, @"^(?:http(?:s)?://)?(?:www(?:[0-9]+)?\.)?", string.Empty, RegexOptions.IgnoreCase);
                    this.Invalidate();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(null, "Error Unknown \n :: " + e.Message + " ::", "Unknown Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }


            return true;

        }

        private bool AddHostsEntry(string ip, string addr, string comment)
        {
            DialogResult dr = MessageBox.Show(null, "This will redirect all web traffic on this PC only from " + txtAddress.Text + " to this local IP Address " + txtIP.Text + "\n\n Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
                return false;

            if(File.ReadAllText(hostsPath).Contains(ip) || File.ReadAllText(hostsPath).Contains(addr))
            {
                MessageBox.Show(null, "An entry for " + ip + " or " + addr + " already exists", "Duplicate Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
                

            string entry = string.Format("{0}   {1}     #{2}", ip, addr, comment);

            if (File.Exists(hostsPath))
            {
                try
                {
                    File.AppendAllText(hostsPath, entry);
                }
                catch (UnauthorizedAccessException uae)
                {
                    MessageBox.Show(null, "Please retry as Admin\n\n :: " + uae.Message + " ::", "Administrator needed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception e)
                {
                    MessageBox.Show(null, "Error Unknown \n :: " + e.Message + " ::", "Unknown Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            if (File.ReadAllText(hostsPath).Contains(addr))
                return true;

            return false;
        }

        private bool VerifyIPAddress()
        {
            if (txtIP.TextLength == 0)
            {
                MessageBox.Show(null, "Please enter an IP", "Null IP string", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            IPAddress taddr = null;
            if (!IPAddress.TryParse(txtIP.Text, out taddr))
            {
                MessageBox.Show(null, "Invalid IP", "IP Parse error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            Ping ping = new Ping();
            if (ping.Send(taddr).Status != IPStatus.Success)
            {
                MessageBox.Show(null, "IP Appears to be not active on your network, please double check it, or this is not required", "Net Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtMsg.Text = "Must be run with Administrator privileges.\n1. Enter a valid local IP address to redirect too.\n2. Enter the url you wish to redirect.\n3. Enter optional comment";
            txtComment.Text = "Loopback for local ERP Server";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show(null, "This will reset your host file to the Windows default host file.  Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                ResetHostsFile();
            }
        }

        private void ResetHostsFile()
        {
            
            try
            {
                File.WriteAllLines(hostsPath, DefaultHostFileString);

                if (File.ReadAllLines(hostsPath).Length == DefaultHostFileString.Length)
                    MessageBox.Show(null, "Reset Complete", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show(null, "Reset Failed, manually reset", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (UnauthorizedAccessException uae)
            {
                MessageBox.Show(null, "Please retry as Admin\n\n :: " + uae.Message + " ::", "Administrator needed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception e)
            {
                MessageBox.Show(null, "Error Unknown \n :: " + e.Message + " ::", "Unknown Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private string[] DefaultHostFileString = {  "# Copyright (c) 1993-2009 Microsoft Corp.","#","# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.","#",
                                                    "# This file contains the mappings of IP addresses to host names. Each",
                                                    "# entry should be kept on an individual line. The IP address should",
                                                    "# be placed in the first column followed by the corresponding host name.",
                                                    "# The IP address and the host name should be separated by at least one",
                                                    "# space.",
                                                    "#",
                                                    "# Additionally, comments (such as these) may be inserted on individual",
                                                    "# lines or following the machine name denoted by a '#' symbol.",
                                                    "#",
                                                    "# For example:",
                                                    "#",
                                                    "#      102.54.94.97     rhino.acme.com          # source server",
                                                    "#       38.25.63.10     x.acme.com              # x client host",
                                                    "",
                                                    "# localhost name resolution is handled within DNS itself.",
                                                    "#	127.0.0.1       localhost",
                                                    "#	::1             localhost" };

        private void resetHostsFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show(null, "This will reset your host file to the Windows default host file.  Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                ResetHostsFile();
            }
        }

        private void viewHostsFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("notepad.exe", hostsPath);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
